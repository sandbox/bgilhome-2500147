<?php

function media_sauce_settings() {
  $form = array();

  // Set defaults for some media variables
  $form['media_wysiwyg_wysiwyg_upload_directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Default upload directory'),
    '#default_value' => variable_get('media_wysiwyg_wysiwyg_upload_directory', 'media'),
  );

  // Default array of image styles to use
  $styles = array(
    'small' => 'Small',
    'medium' => 'Medium',
    'large' => 'Large',
  );

  // Convert to string
  $styles_str = '';
  foreach ($styles as $key => $val) {
    $styles_str .= $key . '|' . $val . "\n";
  }
  $form['#styles_str'] = $styles_str;

  $form['ms_image_styles'] = array(
    '#type' => 'textarea',
    '#title' => t('Image styles'),
    '#description' => t('Image styles to use, one per line in the form key|title. View modes will be created, and media browser embeds restricted to these modes.')
      . t('NB: submitting this form will delete and recreate these styles!!!!'),
    '#default_value' => variable_get('ms_image_styles', $styles_str),
    '#ajax' => array(
      'wrapper' => 'ms-image-sizes',
      'callback' => 'media_sauce_image_sizes_callback',
      'event' => 'blur',
    ),
  );

  $form['sizes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image style sizes'),
    '#prefix' => '<div id="ms-image-sizes">',
    '#suffix' => '</div>',
  );

  // Run AJAX on first render
  $form['sizes'] = media_sauce_image_sizes_callback($form, array());

  $form['#submit'][] = 'media_sauce_settings_submit';

  return system_settings_form($form);
}

// AJAX callback to generate image style width/height/crop fields
function media_sauce_image_sizes_callback($form, $form_state) {
  // Convert styles string back to array
  $styles = isset($form_state['values']['ms_image_styles']) ? $form_state['values']['ms_image_styles'] : variable_get('ms_image_styles', $form['#styles_str']);
  $styles = ds_textarea_to_array($styles);

  // Add fields for image width/height/crop for input image styles
  foreach ($styles as $style_name => $title) {
    $form['sizes']['ms_image_w_' . $style_name] = array(
      '#type' => 'textfield',
      '#maxlength' => 4,
      '#title' => t('@name: width', array('@name' => $style_name)),
      '#default_value' => variable_get('ms_image_w_' . $style_name),
    );
    $form['sizes']['ms_image_h_' . $style_name] = array(
      '#type' => 'textfield',
      '#maxlength' => 4,
      '#title' => t('@name: height', array('@name' => $style_name)),
      '#default_value' => variable_get('ms_image_h_' . $style_name),
    );
    $form['sizes']['ms_image_crop_' . $style_name] = array(
      '#type' => 'checkbox',
      '#title' => t('@name: use crop instead of scale', array('@name' => $style_name)),
      '#default_value' => variable_get('ms_image_crop_' . $style_name),
    );
  }

  return $form['sizes'];
}

// Custom submit for admin settings form
function media_sauce_settings_submit($form, $form_state) {
  // Convert styles string to array
  $styles = isset($form_state['values']['ms_image_styles']) ? $form_state['values']['ms_image_styles'] : array();
  $styles = ds_textarea_to_array($styles);

  // Clear cache so that our hook_entity_view_mode takes action
  // clear_cache_all();

  // Loop through image styles
  foreach ($styles as $style_name => $title) {
    // Delete existing image style
    $style = image_style_load($style_name);
    if (!empty($style)) {
      image_style_delete($style);
    }
    // Create image style
    $style = image_style_save(array(
      'name' => $style_name,
      'label' => $title,
    ));

    // Add effect
    $effect = array(
      'name' => $form_state['values']['ms_image_crop_' . $style_name] ?
        'image_scale_and_crop' : 'image_scale',
      'data' => array(
        'width' => $form_state['values']['ms_image_w_' . $style_name],
        'height' => $form_state['values']['ms_image_h_' . $style_name],
        'upscale' => FALSE,
      ),
      'isid' => $style['isid'],
    );
    image_effect_save($effect);

    // TODO: set file display on Image bundle for view modes

  }

  if (module_exists('media_wysiwyg_view_mode')) {
    // Restrict Media WYSIWYG view modes
    variable_set('media_wysiwyg_view_mode_image_wysiwyg_restricted_view_modes_status', TRUE);
    $entity_info = entity_get_info('file');
    $view_modes = array_keys($entity_info['view modes']);
    $restricted_view_modes = array();
    // Restrict view modes that aren't in $styles
    foreach ($view_modes as $view_mode) {
      if (!array_key_exists($view_mode, $styles)) {
        $restricted_view_modes[] = $view_mode;
      }
    }
    $restricted_view_modes[] = 'default';
    variable_set('media_wysiwyg_view_mode_image_wysiwyg_restricted_view_modes', $restricted_view_modes);
  }
}
